package builder

import (
	"time"

	"bitbucket.org/innius/donkey"
	"bitbucket.org/innius/donkey/internal/logger"
)

type AccountBuilder struct {
	*AccountSpecification
	client *donkey.Client
	logger.Logger
}
type AccountSpecification struct {
	APIKey bool // creates a new API key for the account

	// TTL is the time to live of the account; the account will be deleted automatically if this TTL expires
	TTL time.Duration

	Users []*UserSpecification // create an account with specified users
}

func getClientConfig(opts ...Option) clientConfig {
	options := clientConfig{
		logger: logger.NoOpLogger{},
		client: donkey.New(),
	}
	for i := range opts {
		options = opts[i](options)
	}
	return options
}

func NewAccountBuilder(spec *AccountSpecification, opts ...Option) *AccountBuilder {
	config := getClientConfig(opts...)

	return &AccountBuilder{
		Logger:               config.logger,
		client:               config.client,
		AccountSpecification: spec,
	}
}

func (b *AccountBuilder) Build() (*donkey.Account, error) {
	b.Info("create a new account")
	ttl := b.AccountSpecification.TTL
	if ttl == 0 {
		ttl = donkey.DefaultAccountTTL
	}
	account, err := b.client.CreateAccount(ttl)
	if err != nil {
		return nil, err
	}
	b.Infof("new account %+v", account)
	for range b.Users {
		b.Info("create a new user")
		user, err := b.client.CreateUser(account)
		if err != nil {
			return nil, err
		}
		b.Info("new user created")
		account.Users = append(account.Users, user)
	}

	if b.APIKey {
		b.Info("create an api key")
		apiKey, err := b.client.CreateApiKey(account)
		if err != nil {
			return nil, err
		}
		b.Info("api key created")
		account.ApiKey = apiKey
	}
	account.TTL = ttl
	return account, nil
}
