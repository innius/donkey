package builder

import (
	"bitbucket.org/innius/donkey"
	logger2 "bitbucket.org/innius/donkey/internal/logger"
	"math/rand"
	"time"
)

type sensorValueBuilder struct {
	client *donkey.Client
	sensor *donkey.Sensor
	spec   []*SensorValueSpecification
	logger logger2.Logger
}

func (b *sensorValueBuilder) Build() (donkey.SensorValues, error) {
	b.logger.Infof("create values for %s", b.sensor.ID)
	values := map[donkey.SensorDataTarget][]*donkey.SensorValueRequest{}
	for _, spec := range b.spec {
		values[spec.target()] = append(values[spec.target()], b.flatten(spec)...)
	}
	var result donkey.SensorValues
	for target, values := range values {
		b.logger.Infof("posting %d values to %s", len(values), target)
		v, err := b.client.CreateSensorData(b.sensor, target, values)
		if err != nil {
			return nil, err
		}
		result = append(result, v...)
	}
	b.logger.Info("values created successfully")
	return result, nil
}

func (b *sensorValueBuilder) flatten(spec *SensorValueSpecification) []*donkey.SensorValueRequest {
	var values []*donkey.SensorValueRequest
	if spec.Range != nil {
		values = append(values, spec.Range.expand()...)
	}

	for _, v := range spec.FixedValues {
		ts := v.Timestamp.Unix()
		// kinesis expects timestamps to be in millis :-(
		if spec.Target == donkey.KinesisDataDarget {
			ts = ts * 1000
		}
		values = append(values, &donkey.SensorValueRequest{
			Timestamp: ts,
			Value:     v.Value,
		})
	}

	return values

}

func (spec *SensorValueSpecification) target() donkey.SensorDataTarget {
	if spec.Target != "" {
		return spec.Target
	}
	return donkey.RedisDataTarget
}

func (spec *SensorValueRange) random() float64 {
	return float64(rand.Intn(spec.MaxValue-spec.MinValue+1) + spec.MinValue)
}

func (spec *SensorValueRange) now() time.Time {
	return time.Now()
}

func (spec *SensorValueRange) interval() time.Duration {
	if spec.Interval != 0 {
		return spec.Interval
	}
	return time.Second
}

func (spec *SensorValueRange) expand() []*donkey.SensorValueRequest {
	rand.Seed(time.Now().UnixNano())

	var values []*donkey.SensorValueRequest

	to := spec.now()
	from := to.Add(-spec.Period)

	for current := from; current.Before(to); {
		values = append(values, &donkey.SensorValueRequest{
			Timestamp: current.Unix(),
			Value:     spec.random(),
		})
		current = current.Add(spec.interval())
	}
	return values
}
