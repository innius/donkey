package builder

import (
	"bitbucket.org/innius/donkey"
	logger2 "bitbucket.org/innius/donkey/internal/logger"
	"github.com/samber/lo"
)

func NewMachineBuilder(account *donkey.Account, opts ...Option) *machineBuilder {
	config := getClientConfig(opts...)

	return &machineBuilder{
		logger:  config.logger,
		client:  config.client,
		account: account,
	}
}

type machineBuilder struct {
	logger  logger2.Logger
	client  *donkey.Client
	account *donkey.Account
}

func (b *machineBuilder) Build(spec *MachineSpecification) (*donkey.Machine, error) {
	b.logger.Info("create a new machine")
	machine, err := b.client.CreateMachine(b.account, &donkey.CreateMachineRequest{
		Name:        spec.Name,
		Description: spec.Description,
	})
	if err != nil {
		return nil, err
	}
	demoMachine := lo.SomeBy(spec.Sensors, func(specification *SensorSpecification) bool {
		return len(specification.DemoData) > 0
	})

	b.logger.Infof("machine %+v created", machine)
	if spec.KPI != nil {
		kb := &kpiBuilder{
			client:  b.client,
			machine: machine,
			spec:    spec.KPI,
			logger:  b.logger,
		}
		kpi, err := kb.Build()
		if err != nil {
			return nil, err
		}
		machine.KPI = kpi
	}

	if err := b.buildSensors(machine, spec.Sensors); err != nil {
		return nil, err
	}

	if demoMachine {
		demoData := lo.Map(spec.Sensors, func(specification *SensorSpecification, _ int) lo.Entry[string, []float64] {
			return lo.Entry[string, []float64]{
				Key:   specification.ID,
				Value: specification.DemoData,
			}
		})

		err := b.client.CreateDemoMachine(machine, lo.FromPairs(demoData))
		if err != nil {
			return nil, err
		}
		err = b.client.StartDemoMachine(machine, b.account.TTL)
		if err != nil {
			return nil, err
		}
	}

	for _, rep := range spec.Reports {
		switch c := rep.(type) {
		case donkey.DowntimeReportConfig:
			err := b.client.CreateDowntimeReportConfig(machine, c)
			if err != nil {
				return nil, err
			}
		case donkey.QualityReportConfig:
			err := b.client.CreateQualityReportConfig(machine, c)
			if err != nil {
				return nil, err
			}
		}

	}
	return machine, nil
}

func (b *machineBuilder) buildSensors(m *donkey.Machine, sensors []*SensorSpecification) error {
	for _, spec := range sensors {
		sb := &sensorBuilder{
			client:  b.client,
			machine: m,
			spec:    spec,
			logger:  b.logger,
		}
		sensor, err := sb.Build()
		if err != nil {
			return err
		}
		m.Sensors = append(m.Sensors, sensor)
	}
	return nil
}
