package builder

import (
	"bitbucket.org/innius/donkey"
	logger2 "bitbucket.org/innius/donkey/internal/logger"
	"time"
)

type kpiBuilder struct {
	client  *donkey.Client
	machine *donkey.Machine
	spec    *KPISpecification
	logger  logger2.Logger
}

func (b *kpiBuilder) Build() (*donkey.KPI, error) {
	b.logger.Infof("build kpi %s", b.spec.ID)
	sensors, err := b.buildSensors(b.machine, []*SensorSpecification{
		b.spec.StatusSensor, b.spec.RateSensor, b.spec.BadRateSensor,
	})
	if err != nil {
		return nil, err
	}

	b.machine.Sensors = append(b.machine.Sensors, sensors...)

	kpi, err := b.client.CreateKPI(b.machine, &donkey.CreateKPIRequest{
		ID:             string(b.spec.ID),
		StatusSensor:   sensors[0],
		RateSensor:     sensors[1],
		BadRateSensor:  sensors[2],
		FixedRateValue: b.spec.TheoreticalFixedRateValue,
		Thresholds:     nil,
	})
	b.logger.Info("kpi created successfully")

	if err != nil {
		return nil, err
	}

	time.Sleep(5 * time.Second) // Bah, required for redis-timeseries-schema-updater to create the keys

	if b.spec.Calculate {
		if err := b.client.RefreshKPI(kpi); err != nil {
			return nil, err
		}
	}

	return kpi, nil
}

func (b *kpiBuilder) buildSensors(m *donkey.Machine, sensors []*SensorSpecification) ([]*donkey.Sensor, error) {
	var result []*donkey.Sensor
	for _, spec := range sensors {
		sb := &sensorBuilder{
			client:  b.client,
			machine: b.machine,
			spec:    spec,
			logger:  b.logger,
		}
		sensor, err := sb.Build()
		if err != nil {
			return nil, err
		}
		result = append(result, sensor)
	}
	return result, nil
}
