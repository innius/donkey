package builder

import (
	"fmt"
	"time"

	"bitbucket.org/innius/donkey"
	logger2 "bitbucket.org/innius/donkey/internal/logger"
	"github.com/samber/lo"
)

type sensorBuilder struct {
	client  *donkey.Client
	machine *donkey.Machine
	spec    *SensorSpecification
	logger  logger2.Logger
}

func (b *sensorBuilder) Build() (*donkey.Sensor, error) {
	b.logger.Infof("create sensor %s", b.spec.ID)
	aliases := lo.MapValues(b.spec.ValueMapping, func(v string, k float64) donkey.SensorValueAlias {
		return donkey.SensorValueAlias{
			Value:       int(k),
			Description: v,
		}
	})

	sensor, err := b.client.CreateSensor(b.machine, donkey.CreateSensorRequest{
		PhysicalID:     b.spec.ID,
		ID:             b.spec.ID,
		Name:           b.spec.Name,
		Description:    b.spec.Description,
		Kind:           b.kind(),
		Rate:           b.rate(),
		Missing:        b.spec.MissingValueStrategy,
		Type:           donkey.SensorDatatypeDouble,
		SensorDataType: string(b.spec.SensorType),
		Visible:        b.spec.Visible,
		Aliases:        lo.Values(aliases),
		Recipe:         b.spec.Recipe,
		BatchSensor:    b.spec.BatchSensor,
	})

	if err != nil {
		return nil, err
	}

	b.logger.Infof("sensor %+v created", sensor)
	if b.spec.Data != nil {
		vb := &sensorValueBuilder{
			logger: b.logger,
			client: b.client,
			sensor: sensor,
			spec:   b.spec.Data,
		}

		values, err := vb.Build()
		if err != nil {
			return nil, err
		}
		sensor.Values = values
	}

	for _, spec := range b.spec.Triggers {
		tb := &triggerBuilder{
			client:  b.client,
			machine: b.machine,
			sensor:  sensor,
			spec:    &spec,
		}
		trigger, err := tb.Build()
		if err != nil {
			return nil, err
		}
		sensor.Triggers = append(sensor.Triggers, trigger)
	}

	return sensor, nil
}

func (b *sensorBuilder) kind() string {
	if b.spec.Kind != "" {
		return b.spec.Kind
	}
	return "physical"
}

func (b *sensorBuilder) rate() int {
	switch b.spec.Rate {
	case time.Minute:
		return 60
	case time.Hour:
		return 3600
	case 10 * time.Second:
		return 10
	case time.Second, 0:
		return 1
	default:
		panic(fmt.Sprintf("sensor rate %+v is not supported", b.spec.Rate))
	}
}
