package builder

import "bitbucket.org/innius/donkey"

type triggerBuilder struct {
	client  *donkey.Client
	machine *donkey.Machine
	sensor  *donkey.Sensor
	spec    *TriggerSpecification
}

func (b *triggerBuilder) Build() (*donkey.Trigger, error) {
	trigger, err := b.client.CreateTrigger(donkey.CreateTriggerRequest{
		Machine: b.machine,
		Sensor:  b.sensor,
		Trigger: &donkey.Trigger{
			ID:                "",
			Notification:      b.spec.Notification,
			DurationInSeconds: b.spec.DurationInSeconds,
			Debouncer:         b.spec.Debouncer,
			//Condition:           b.spec.Condition,
			Type: b.spec.Type,
			ThresholdExpression: donkey.ThresholdExpression{
				Expr:     b.spec.ThresholdExpression.Expr,
				IsSensor: b.spec.ThresholdExpression.IsSensor,
				SensorID: b.spec.ThresholdExpression.SensorID,
				Value:    b.spec.ThresholdExpression.Value,
			},
		},
	})

	return trigger, err
}
