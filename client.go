package donkey

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httputil"
	"os"
	"strconv"
	"time"

	"bitbucket.org/innius/donkey/internal/logger"
	"github.com/jinzhu/now"
	"github.com/vrischmann/envconfig"
)

type BaseUrl string

// New returns a new client instance with the provided options
func New(opts ...Option) *Client {

	client := &Client{
		httpClient:   DefaultHttpClient,
		ClientConfig: &ClientConfig{},
		logger:       logger.NoOpLogger{},
	}

	// apply the list of options to Server
	for _, opt := range opts {
		opt(client)
	}

	if err := envconfig.Init(client.ClientConfig); err != nil {
		client.logger.Info(err.Error())
	}

	if client.ApiKey == "" {
		client.ApiKey = os.Getenv("INNIUS_API_KEY")
	}

	return client
}

type Option func(c *Client)

// Endpoint defines the endpoint for the innius testing backend
func Endpoint(s string) Option {
	return func(c *Client) {
		c.Endpoint = BaseUrl(s)
	}
}

// WithApiKey defines the api-key for the innius test backend of the client
func WithApiKey(s string) Option {
	return func(c *Client) {
		c.ApiKey = s
	}
}

// Debug enables debugging of all http requests / responses
func Debug(debug bool) Option {
	return func(c *Client) {
		c.Debug = debug
	}
}

func Logger(logger logger.Logger) Option {
	return func(c *Client) {
		c.logger = logger
	}
}
func (e BaseUrl) URL(path string, a ...interface{}) string {
	return string(e) + fmt.Sprintf(path, a...)
}

var DefaultHttpClient = &http.Client{
	Timeout: 60 * time.Second,
}

// CI verifies if the current environment is a CI environment
// This environment variable has to be enabled during build
// Which is the case if https://github.com/innius/aws-codebuild-extras is injected into the code build environment
func CI() bool {
	if v, found := os.LookupEnv("CI"); found {
		ci, _ := strconv.ParseBool(v)
		return ci
	}
	return false
}

type ClientConfig struct {
	ApiKey             string  `envconfig:"optional"`
	SensorsEndpoint    BaseUrl `envconfig:"default=https://test.innius.com/sensor-service"`
	DemoMachineService BaseUrl `envconfig:"default=https://test.innius.com/demomachine-service"`
	AggregationService BaseUrl `envconfig:"default=https://test.innius.com/aggregation-service"`
	KPIService         BaseUrl `envconfig:"default=https://test.innius.com/kpi-service"`
	APIKeyService      BaseUrl `envconfig:"default=https://api.test.innius.com/apikeys"`
	MachineService     BaseUrl `envconfig:"default=https://test.innius.com/machine-service"`
	Endpoint           BaseUrl `envconfig:"default=https://test.innius.com/postman"`
}

// the Client
type Client struct {
	*ClientConfig
	Debug      bool
	httpClient *http.Client
	logger     logger.Logger
}

const apiKeyHeader = "x-api-key"

type CreateAccountOptions struct {
	CreateDefaultMachine bool

	// TTL defines how long the account must retain
	TTL time.Duration
}

type signupResponseDto struct {
	UserID      string `json:"user_id"`
	Email       string `json:"email"`
	Password    string `json:"password"`
	AccessToken string `json:"token"`
	MachineID   string `json:"machine_id"`
	Domain      string `json:"domain"`
	CompanyID   string `json:"company_id"`
	CompanyName string `json:"company_name"`
}

func (c *Client) error(err error) error {
	return err
}

func (c *Client) CreateAccountWithOptions(opts *CreateAccountOptions) (*Account, error) {
	resp := &signupResponseDto{}
	if err := c.post(c.Endpoint.URL(fmt.Sprintf("/accounts/signup?lifespan=%d&create_default_machine=%t", int64(opts.TTL.Minutes()), opts.CreateDefaultMachine)), nil, http.StatusOK, &resp, nil); err != nil {
		return nil, c.error(err)
	}
	account := &Account{
		Company: &Company{
			Domain:      resp.Domain,
			CompanyID:   resp.CompanyID,
			CompanyName: resp.CompanyName,
		},
	}

	account.Admin = &User{
		Account: account,
		Credentials: &Credentials{
			AccessToken: resp.AccessToken,
			Password:    resp.Password,
		},
		UserID: resp.UserID,
		Email:  resp.Email,
	}

	return account, nil
}

const DefaultAccountTTL = 90 * time.Minute

// CreateAccount create a new innius account with a company and administrator
func (c *Client) CreateAccount(ttl time.Duration) (*Account, error) {
	return c.CreateAccountWithOptions(&CreateAccountOptions{
		CreateDefaultMachine: false,
		TTL:                  ttl,
	})
}

func (c *Client) post(path string, body interface{}, status int, v interface{}, authFn authentication) error {
	req, err := c.newRequest(http.MethodPost, path, body, authFn)
	if err != nil {
		return err
	}
	return c.do(req, status, v)
}

type authentication func(*http.Request)

func bearerTokenAuthentication(token string) func(r *http.Request) {
	return func(r *http.Request) {
		r.Header.Add(http.CanonicalHeaderKey("authorization"), fmt.Sprintf("Bearer %s", token))
	}
}

func (c *Client) newRequest(method string, url string, body interface{}, auth func(r *http.Request)) (*http.Request, error) {
	b := &bytes.Buffer{}
	if body != nil {
		if err := json.NewEncoder(b).Encode(body); err != nil {
			return nil, err
		}
	}

	req, _ := http.NewRequest(method, url, b)
	if auth != nil {
		auth(req)
	} else {
		req.Header.Add(apiKeyHeader, c.ApiKey)
	}
	return req, nil
}

func (c *Client) do(r *http.Request, status int, v interface{}) error {
	if c.Debug {
		if b, err := httputil.DumpRequestOut(r, true); err == nil {
			c.logger.Debug(string(b))
		}
	}
	resp, err := c.httpClient.Do(r)
	if err != nil {
		return err
	}
	if c.Debug {
		if b, err := httputil.DumpResponse(resp, true); err == nil {
			c.logger.Debug(string(b))
		}
	}
	defer resp.Body.Close()
	if resp.StatusCode != status {
		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		return fmt.Errorf("%s returned unexpected status code: %d, response body: %s", r.URL.String(), resp.StatusCode, buf.String())
	}
	if v != nil {
		return json.NewDecoder(resp.Body).Decode(v)
	}
	return nil
}

// CreateUser adds a new user to an existing account
func (c *Client) CreateUser(account *Account) (*User, error) {
	resp := &signupResponseDto{}

	path := c.Endpoint.URL("/accounts")

	req, err := c.newRequest(http.MethodPost, path, account.Company, nil)
	if err != nil {
		return nil, c.error(err)
	}

	if err := c.do(req, http.StatusOK, resp); err != nil {
		return nil, c.error(err)
	}

	return &User{
		Account: account,
		Credentials: &Credentials{
			AccessToken: resp.AccessToken,
			Password:    resp.Password,
		},
		UserID: resp.UserID,
		Email:  resp.Email,
	}, nil
}

type ApiKey struct {
	ID     string `json:"id"`
	Secret string `json:"secret"`
}

func (c *Client) CreateApiKey(account *Account) (*ApiKey, error) {
	payload := struct {
		Description string   `json:"description"`
		Scopes      []string `json:"scopes"`
	}{
		Description: "dashboard-integration-test",
		Scopes:      nil,
	}

	req, err := c.newRequest(http.MethodPost, c.APIKeyService.URL("/keys"), payload, bearerTokenAuthentication(account.Admin.Credentials.AccessToken))
	if err != nil {
		return nil, c.error(err)
	}
	resp := &ApiKey{}
	if err := c.do(req, http.StatusCreated, resp); err != nil {
		return nil, c.error(err)
	}
	return resp, nil
}

type CreateMachineRequest struct {
	Name        string `json:"displayname"`
	Description string `json:"description"`
}

// CreateMachine creates a new machine for an existing account
func (c *Client) CreateMachine(acc *Account, in *CreateMachineRequest) (*Machine, error) {
	dto := struct {
		MachineID   string `json:"id"`
		Description string `json:"description"`
		Name        string `json:"displayname"`
	}{}
	uri := c.MachineService.URL(fmt.Sprintf("/companies/%s/machines", acc.Company.CompanyID))

	req, err := c.newRequest(http.MethodPost, uri, in, bearerTokenAuthentication(acc.Admin.Credentials.AccessToken))
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		return nil, c.error(err)
	}

	if err := c.do(req, http.StatusCreated, &dto); err != nil {
		return nil, c.error(err)
	}

	return &Machine{
		ID:      dto.MachineID,
		Account: acc,
	}, nil
}

const SensorDatatypeDouble = "double"

type SensorValueAlias struct {
	Value       int    `json:"value"`
	Description string `json:"description"`
}

// CreateSensorRequest defines the specification of a sensor
type CreateSensorRequest struct {
	PhysicalID     string             `json:"physicalid"`
	ID             string             `json:"id"`
	Name           string             `json:"name"`
	Description    string             `json:"description"`
	Kind           string             `json:"kind"`
	BatchSensor    string             `json:"batch"`
	Recipe         string             `json:"recipe"`
	Rate           int                `json:"rate"`
	Type           string             `json:"type"`
	Visible        bool               `json:"visible"`
	Missing        string             `json:"missing"`
	DefaultValue   float64            `json:"defaultvalue"`
	Aliases        []SensorValueAlias `json:"aliases"`
	SensorDataType string             `json:"sensor_data_type"`
}

// CreateSensor creates a new sensor for an existing machine according to the provided specification
func (c *Client) CreateSensor(m *Machine, spec CreateSensorRequest) (*Sensor, error) {
	type dto struct {
		PhysicalID  string `json:"physicalid"`
		ID          string `json:"id"`
		Name        string `json:"name"`
		Description string `json:"description"`
		Kind        string `json:"kind"`
		BatchSensor string `json:"batch"`
		Rate        int    `json:"rate"`
		Visible     bool   `json:"visible"`
		Value       struct {
			Type           string  `json:"type"`
			SensorDataType string  `json:"sensor_data_type"`
			Missing        string  `json:"missing"`
			DefaultValue   float64 `json:"defaultvalue"`
		}
		Aliases []SensorValueAlias `json:"aliases"`
		Recipe  string             `json:"recipe"`
	}

	payload := dto{
		PhysicalID:  spec.PhysicalID,
		ID:          spec.ID,
		Name:        spec.Name,
		Description: spec.Description,
		Kind:        spec.Kind,
		BatchSensor: spec.BatchSensor,
		Rate:        spec.Rate,
		Aliases:     spec.Aliases,
		Visible:     spec.Visible,
		Recipe:      spec.Recipe,
	}
	payload.Value.Type = spec.Type
	payload.Value.DefaultValue = spec.DefaultValue
	payload.Value.Missing = spec.Missing
	payload.Value.SensorDataType = spec.SensorDataType

	path := c.SensorsEndpoint.URL(fmt.Sprintf("/%s/eventing/definition", m.ID))

	req, err := c.newRequest(http.MethodPost, path, []dto{payload}, bearerTokenAuthentication(m.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return nil, c.error(err)
	}

	var res []CreateSensorRequest
	if err := c.do(req, http.StatusOK, &res); err != nil {
		return nil, c.error(err)
	}

	return &Sensor{
		Machine:     m,
		ID:          res[0].ID,
		Name:        res[0].Name,
		Kind:        res[0].Kind,
		BatchSensor: res[0].BatchSensor,
		Rate:        res[0].Rate,
	}, nil
}

type ScriptSensor struct {
	ID    string `json:"sensorId"`
	Alias string `json:"alias"`
}

type ScriptRequest struct {
	Machine     *Machine       `json:"-"`
	Body        string         `json:"body"`
	Description string         `json:"description"`
	ID          string         `json:"id"`
	Active      bool           `json:"active"`
	Rate        int            `json:"rate"`
	Input       []ScriptSensor `json:"input"`
	Output      []ScriptSensor `json:"output"`
}

type ScriptResponse struct {
	Body        string         `json:"body"`
	Description string         `json:"description"`
	ID          string         `json:"id"`
	Active      bool           `json:"active"`
	Input       []ScriptSensor `json:"input"`
	Output      []ScriptSensor `json:"output"`
}

func (c *Client) PostScript(spec ScriptRequest) error {
	path := c.SensorsEndpoint.URL(fmt.Sprintf("/%s/eventing/scripts/v2", spec.Machine.ID))

	req, err := c.newRequest(http.MethodPost, path, spec, bearerTokenAuthentication(spec.Machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}

	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}

	return nil
}

type InstallScriptRequest struct {
	Machine  *Machine
	ScriptID string
}

func (c *Client) InstallScript(spec InstallScriptRequest) error {
	path := c.SensorsEndpoint.URL(fmt.Sprintf("/%s/eventing/scripts/%s/install", spec.Machine.ID, spec.ScriptID))

	req, err := c.newRequest(http.MethodPost, path, nil, bearerTokenAuthentication(spec.Machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}

	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}

	return nil
}

type ListScriptsRequest struct {
	Machine *Machine
}

func (c *Client) ListScripts(spec ListScriptsRequest) ([]ScriptResponse, error) {
	path := c.SensorsEndpoint.URL(fmt.Sprintf("/%s/eventing/scripts", spec.Machine.ID))

	req, err := c.newRequest(http.MethodGet, path, nil, bearerTokenAuthentication(spec.Machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return nil, c.error(err)
	}

	var res []ScriptResponse
	if err := c.do(req, http.StatusOK, &res); err != nil {
		return nil, c.error(err)
	}

	return res, nil
}

type CreateTriggerRequest struct {
	Machine *Machine `json:"-"`
	Sensor  *Sensor  `json:"-"`
	*Trigger
}

// CreateTrigger creates a new trigger
func (c *Client) CreateTrigger(spec CreateTriggerRequest) (*Trigger, error) {

	path := c.SensorsEndpoint.URL(fmt.Sprintf("/%s/eventing/definition/%s/triggers", spec.Machine.ID, spec.Sensor.ID))

	req, err := c.newRequest(http.MethodPost, path, spec, bearerTokenAuthentication(spec.Machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return nil, c.error(err)
	}

	if err := c.do(req, http.StatusOK, nil); err != nil {
		return nil, c.error(err)
	}

	return spec.Trigger, nil
}

// SensorValueRequest represents a sensor time stamped value which is pumped into either redshift or kinesis
type SensorValueRequest struct {
	Timestamp int64   `json:"timestamp"`
	Value     float64 `json:"value"`
}

func (c *Client) PostKinesisValue(s *Sensor, values ...*SensorValueRequest) (SensorValues, error) {
	return c.CreateSensorData(s, KinesisDataDarget, values)
}

// CreateSensorData posts specified sensor values to a target
func (c *Client) CreateSensorData(s *Sensor, target SensorDataTarget, values []*SensorValueRequest) (SensorValues, error) {
	url := c.Endpoint.URL(fmt.Sprintf("/machines/%s/sensors/%s/values/%s", s.Machine.ID, s.ID, target))
	req, err := c.newRequest(http.MethodPost, url, values, bearerTokenAuthentication(s.Machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return nil, c.error(err)
	}
	if err := c.do(req, http.StatusOK, nil); err != nil {
		return nil, c.error(err)
	}

	result := make(SensorValues, len(values))
	for i, v := range values {
		result[i] = &SensorValue{
			Timestamp: now.New(time.Unix(v.Timestamp, 0)),
			Value:     v.Value,
		}
	}
	return result, nil
}

// KPISpecification specifies how a kpi is created
type CreateKPIRequest struct {
	ID             string
	StatusSensor   *Sensor
	RateSensor     *Sensor
	BadRateSensor  *Sensor
	FixedRateValue int
	Thresholds     []KPIThreshold
}

func (c *Client) CreateKPI(machine *Machine, spec *CreateKPIRequest) (*KPI, error) {
	type dto struct {
		Availability struct {
			StatusSensor string `json:"status_sensor"`
		} `json:"availability"`
		Production struct {
			RateSensor string `json:"rate_sensor"`
			Frequency  int    `json:"frequency"`
		} `json:"production"`
		Theoretical struct {
			FixedRateValue int `json:"rate_fixed_value"`
			Frequency      int `json:"frequency"`
		} `json:"theoretical"`
		Quality struct {
			BadRateSensor string `json:"bad_rate_sensor"`
			Frequency     int    `json:"frequency"`
		} `json:"quality"`
		Thresholds []KPIThreshold `json:"thresholds"`
	}

	payload := dto{}
	payload.Availability.StatusSensor = spec.StatusSensor.ID
	payload.Production.Frequency = spec.RateSensor.Rate
	payload.Production.RateSensor = spec.RateSensor.ID
	payload.Theoretical.Frequency = spec.RateSensor.Rate
	payload.Theoretical.FixedRateValue = spec.FixedRateValue
	payload.Quality.BadRateSensor = spec.BadRateSensor.ID
	payload.Quality.Frequency = spec.BadRateSensor.Rate
	payload.Thresholds = spec.Thresholds

	url := c.KPIService.URL(fmt.Sprintf("/%s/config/%s", machine.ID, spec.ID))
	req, err := c.newRequest(http.MethodPost, url, payload, bearerTokenAuthentication(machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return nil, c.error(err)
	}
	if err := c.do(req, http.StatusOK, nil); err != nil {
		return nil, c.error(err)
	}
	return &KPI{
		ID:             spec.ID,
		StatusSensor:   spec.StatusSensor,
		RateSensor:     spec.RateSensor,
		BadRateSensor:  spec.BadRateSensor,
		FixedRateValue: spec.FixedRateValue,
		Thresholds:     spec.Thresholds,
		Machine:        machine,
	}, nil
}

// RefreshKPI refreshes the kpi
func (c *Client) RefreshKPI(kpi *KPI) error {
	url := c.KPIService.URL(fmt.Sprintf("/%s/refresh", kpi.Machine.ID))
	req, err := c.newRequest(http.MethodPut, url, nil, bearerTokenAuthentication(kpi.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}
	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}
	return nil
}

func (c *Client) CreateDemoMachine(machine *Machine, demoData map[string][]float64) error {
	url := c.DemoMachineService.URL(fmt.Sprintf("/%s", machine.ID))
	req, err := c.newRequest(http.MethodPost, url, nil, bearerTokenAuthentication(machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}
	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}

	type dto struct {
		ID     string    `json:"id"`
		Name   string    `json:"name"`
		Values []float64 `json:"values"`
	}
	var payload []dto
	for key, v := range demoData {
		payload = append(payload, dto{ID: key, Name: key, Values: v})
	}

	req, err = c.newRequest(http.MethodPut, url, payload, bearerTokenAuthentication(machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}

	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}

	return nil
}

func (c *Client) StartDemoMachine(machine *Machine, duration time.Duration) error {
	url := c.DemoMachineService.URL(fmt.Sprintf("/%s/start?duration=%s", machine.ID, duration.Hours()))
	req, err := c.newRequest(http.MethodPost, url, nil, bearerTokenAuthentication(machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}
	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}
	return nil
}

type DowntimeReportConfig struct {
	DowntimeSensorID string `json:"downtime_sensor_id"`
	Name             string `json:"name"`
}


func (c *Client) CreateDowntimeReportConfig(machine *Machine, cfg DowntimeReportConfig) error {
	url := c.AggregationService.URL(fmt.Sprintf("/machine/%s/downtime/configs/", machine.ID))
	req, err := c.newRequest(http.MethodPost, url, cfg, bearerTokenAuthentication(machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}
	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}
	return nil
}

type QualityReportConfig struct {
	Name                    string   `json:"name"`
	BatchSensorID           string   `json:"batch_sensor_id"`
	TotalProductionSensorID string   `json:"total_production_sensor_id"`
	RejectionSensorIDs      []string `json:"rejection_sensor_ids"`
}


func (c *Client) CreateQualityReportConfig(machine *Machine, cfg QualityReportConfig) error {
	url := c.AggregationService.URL(fmt.Sprintf("/machine/%s/quality/configs/", machine.ID))
	req, err := c.newRequest(http.MethodPost, url, cfg, bearerTokenAuthentication(machine.Account.Admin.Credentials.AccessToken))
	if err != nil {
		return c.error(err)
	}
	if err := c.do(req, http.StatusOK, nil); err != nil {
		return c.error(err)
	}
	return nil
}