# donkey

![alt text](docs/img/donkey.png)

Donkey is a utility package that enables easy integration testing of innius services.
It sets up companies, machines, sensors, and more.

## Used by

Innius services with integration tests that make use of Donkey:

* [timeseries-query-api](https://bitbucket.org/innius/timeseries-query-api/src/master/)
* [logistics-service](https://bitbucket.org/to-increase/logistics-service/src/master/)
* [collateral-service-v2](https://bitbucket.org/innius/collateral-service-v2/src/master/)
* [innius-api](https://bitbucket.org/innius/api/src/master/)

## Usage

### Installation
```
go get "bitbucket.org/innius/donkey"
```
### Environment Variables

| Variable  | Description |
|---|---|
| `CI`       | Continuous Integration flag, can be used to skip integration tests; defaults to false
| `APIKEY`   | API key for innius testing backend
| `ENDPOINT` | endpoint of the innius testing backend; defaults to test environment
| `SENSORS_ENDPOINT` | endpoint of the sensor service; defaults to test environment
| `APIKEY_SERVICE` | the endpoint of the innius API key service; defaults to test environment
| `KPI_SERVICE` | the endpoint of the KPI service; defaults to test environment
| `MACHINE_SERVICE` | the endpoint of the machine service; defaults to test environment

### Example

```golang
// main_test.go
package integration

import (
	"os"
	"testing"
	"time"

	"bitbucket.org/innius/donkey"
	"bitbucket.org/innius/donkey/builder"
	log "github.com/sirupsen/logrus"
)

func TestMain(m *testing.M) {
    logger := log.StandardLogger()
    logger.SetLevel(log.DebugLevel)
    logger.SetFormatter(&log.TextFormatter{})

    client := donkey.New(donkey.Debug(false), donkey.Logger(logger))
    log.Info("testing")

    b := builder.New(client, &builder.Specification{
        Machine: &builder.MachineSpecification{
            KPI: builder.DefaultOEESpecification(),
            Sensors: []*builder.SensorSpecification{
                {
                    ID: "S1",
                    Data: []*builder.SensorValueSpecification{
                        {
                            Target: donkey.RedshiftDataTarget,
                            Range: &builder.SensorValueRange{
                                Period:   time.Minute,
                                Interval: 3 * time.Second,
                                MaxValue: 10,
                                MinValue: 20,
                            },
                            FixedValues: nil,
                        },
                    },
                },
            },
        },
        APIKey: true,
        Users:  []*builder.UserSpecification{},
    })

    account, err := b.Build()
    if err != nil {
        panic(err)
    }

	log.Info("%+v", account)

    os.Exit(m.Run())
}
```

Make sure to set the `APIKEY` environment variable. For example, 
run the following command:
```
API_KEY=YOUR_API_KEY go test -v ./...
```
