package donkey

import (
	"fmt"
	"net/http"
	"time"

	"github.com/jinzhu/now"
)

// Account defines a complete innius account which can be used in tests
// A variable of this type is typically declared as a global package var which can
// be used in multiple tests
type Account struct {
	// Company describe the company details
	Company *Company

	// Admin is the administrator of the account
	// The administrator is not added to the Users slice
	Admin *User

	// Machine describes the machine created for this account
	Machine *Machine

	// ApiKey defines the company API key
	ApiKey *ApiKey

	// Users defines additional users of the company
	Users []*User

	// The TTL of the account
	TTL time.Duration
}

// Company maintains the company details
type Company struct {
	// Domain defines the domain of the company
	Domain string `json:"domain"`
	// CompanyID defines the internal id of the company
	CompanyID string `json:"company_id"`
	// CompanyName defines the company name
	CompanyName string `json:"company_name"`
}

// Credentials define the security credentials of a user
type Credentials struct {
	// AccessToken is the auth0 access token which can be used as a Bearer token to backend requests
	AccessToken string
	// The auth0 password
	Password string `json:"password"`
}

// Bearer returns the access token as an authorization header with a bearer token
func (c Credentials) Bearer() (string, string) {
	return http.CanonicalHeaderKey("authorization"), fmt.Sprintf("Bearer %s", c.AccessToken)
}

// User represents an innius user
// User embeds the Account to enable access to account details anywhere the user is used
type User struct {
	*Account
	// Credentials are the credentials of the user
	Credentials *Credentials
	// UserID is the innius user id
	UserID string `json:"user_id"`
	// Email is the email address of the user
	Email string `json:"email"`
}

// Machine represents an innius machine
type Machine struct {
	// ID is the internal ID (trn) of the machine
	ID string
	// The account which owns the machine
	Account *Account
	// Sensors is the list of sensors defined for this machine
	Sensors SensorList
	// The KPI defined for this machine
	// currently we support only one kpi for a machine
	KPI *KPI
}

// SensorList represents a list of sensors
type SensorList []*Sensor

// Get returns a sensor by its sensor id
func (sl SensorList) Get(id string) *Sensor {
	for i := range sl {
		if sl[i].ID == id {
			return sl[i]
		}
	}
	return nil
}

// Sensor represents an innius sensor
type Sensor struct {
	// Machine represents the associated machine of the sensor
	Machine *Machine
	// ID is the internal id
	ID string `json:"id"`
	// Name is the friendly name of the sensor
	Name string `json:"name"`
	// Kind defines if a sensor is manual, virtual or physical
	Kind string `json:"kind"`
	// The batch sensor ID associated to this sensor
	BatchSensor string `json:"batch"`
	// The rate of the sensor in seconds
	Rate int `json:"rate"`
	// The values created for the sensor
	Values SensorValues
	// The triggers of this sensor
	Triggers []*Trigger
}

// KPI defines a KPI
type KPI struct {
	// The ID of the sensor (oee, mttr or mtbf)
	ID string

	// StatusSensor is the status sensor (oee)
	StatusSensor *Sensor

	// RateSensor is the rate sensor (oee)
	RateSensor *Sensor

	// BadRateSensor is the quality sensor (oee)
	BadRateSensor *Sensor

	// FixedRateValue is the theoretical rate of the machine (oee)
	FixedRateValue int

	Thresholds []KPIThreshold
	// Machine defines the machine for which the kpi is defined
	*Machine
}

//  KPIThreshold define the thresholds of a kpi
type KPIThreshold struct {
	Status int `json:"status"`
	Value  int `json:"value"`
}

type Trigger struct {
	ID                  string              `json:"id"`
	Notification        bool                `json:"notification"`
	DurationInSeconds   int                 `json:"durationInSeconds"`
	Debouncer           int                 `json:"debouncer"`
	Condition           string              `json:"condition"`
	Type                string              `json:"type"`
	ThresholdExpression ThresholdExpression `json:"thresholdExpression"`
}

type ThresholdExpression struct {
	Expr     string  `json:"expr"`
	IsSensor bool    `json:"isSensor"`
	SensorID string  `json:"sensorid"`
	Value    float64 `json:"value"`
}

// SensorDataTarget defines the target of the sensor event values
type SensorDataTarget string

const (
	RedisDataTarget   SensorDataTarget = "redis"
	KinesisDataDarget SensorDataTarget = "kinesis"
)

// SensorValue represents a timestamped sensor value
type SensorValue struct {
	Timestamp *now.Now
	Value     float64
}

// SensorValues defines a slice of sensor values
type SensorValues []*SensorValue

// Head returns the first element from the sensor values slice
func (ds SensorValues) Head() *SensorValue {
	if len(ds) == 0 {
		return nil
	}
	return ds[0]
}

// Head returns the last element from the sensor values slice
func (ds SensorValues) Tail() *SensorValue {
	if len(ds) == 0 {
		return nil
	}
	return ds[len(ds)-1]
}

// StartTime returns the start time of the slice of sensor values
func (ds SensorValues) StartTime() *now.Now {
	return ds.Head().Timestamp
}

// EndTime returns the end time of the slice of sensor values
func (ds SensorValues) EndTime() *now.Now {
	return ds.Tail().Timestamp
}
