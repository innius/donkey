module bitbucket.org/innius/donkey

go 1.18

require (
	github.com/jinzhu/now v1.1.1
	github.com/samber/lo v1.25.0
	github.com/vrischmann/envconfig v1.2.0
)

require (
	github.com/stretchr/testify v1.7.1 // indirect
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
