package logger

type NoOpLogger struct{}

func (n NoOpLogger) Debug(args ...interface{}) {
}

func (n NoOpLogger) Debugf(template string, args ...interface{}) {
}

func (n NoOpLogger) Info(args ...interface{}) {
}

func (n NoOpLogger) Infof(template string, args ...interface{}) {
}

type Logger interface {
	Info(args ...interface{})
	Infof(template string, args ...interface{})
	Debug(args ...interface{})
	Debugf(template string, args ...interface{})
}
