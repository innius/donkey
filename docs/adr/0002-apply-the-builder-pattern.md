# 2. apply the builder pattern

Date: 2020-02-06

## Status

Accepted

## Context

Constructing a full scenario including api keys, kpis and multiple sensors was getting too complicated to accomplish this with separate api calls.
A builder pattern seems to be more suitable for these requirements.

## Decision

Apply the builder pattern to this package.

## Consequences

This is a breaking changes which affects all consumers of this package.
